﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace Daggerfall { 
    public class Config { 
        private static Config _instance;

        private const string configPath = "DaggerfallUnity.xml";

        public static Config Instance { 
            get { 
                if (_instance == null) {
                    _instance = Config.Deserialize(configPath);
                    if (_instance == null) { 
                        _instance = new Config();
                    }
                    _instance.Save();
                }

                return _instance;
            }
        }

        public void OnPreSerialize() {}
        public void OnPostDeserialize() {}

        public void Save() { 
            Config.Serialize(configPath, _instance);
        }

        // Thanks to First Person Camera Mod for Cities: Skylines for this approach
        public static void Serialize(string filename, Config config)
        {
            var serializer = new XmlSerializer(typeof(Config));

            using (var writer = new StreamWriter(filename))
            {
                config.OnPreSerialize();
                serializer.Serialize(writer, config);
            }
        }

        public static Config Deserialize(string filename)
        {
            var serializer = new XmlSerializer(typeof(Config));

            try
            {
                using (var reader = new StreamReader(filename))
                {
                    var config = (Config)serializer.Deserialize(reader);
                    config.OnPostDeserialize();
                    return config;
                }
            }
            catch { }

            return null;
        }
    }
}
